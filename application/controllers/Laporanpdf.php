<?php
Class Laporanpdf extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
    }
    
    function index(){
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(190,7,'TOKO JAYA ABADI Cabang Jakut',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'Laporan Pembelian',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,6,'ID Pembelian',1,0);
        $pdf->Cell(85,6,'NO Trans',1,0);
        $pdf->Cell(27,6,'Total Barang',1,0);
        $pdf->Cell(25,6,'Total QTY',1,1);
        $pdf->Cell(25,6,'Jumlah Nominal Pembelian',1,1);
        $pdf->SetFont('Arial','',10);
        $mahasiswa = $this->db->get('mahasiswa')->result();
        foreach($data_pembelian_detail as $data){
            $pdf->Cell(20,6,$data->id_pembelian_h,1,0);
            $pdf->Cell(85,6,$data->no_transaksi,1,0);
            $pdf->Cell(27,6,$data->jumlah,1,0);
            $pdf->Cell(25,6,$data->qty,1,1);
            // $pdf->Cell(25,6,$data->Rp. number_format($data->jumlah) 1,1); 
        }
        $pdf->Output();
    }
}