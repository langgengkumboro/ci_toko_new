<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_models extends CI_Model
{
   //panggil nama table
    private $_table_header = "pembelian_header";
    private $_table_detail = "pembelian_detail";

    public function tampilDataPembelian()
        {
            $query  = $this->db->query(
                "SELECT * FROM " . $this->_table_header . " WHERE flag = 1"
            );
            return $query->result();  
        }

    public function savePembelianHeader()
        {
            $data['no_transaksi']   = $this->input->post('no_transaksi');
            $data['kode_supplier']  = $this->input->post('kode_supplier');
            $data['tanggal']        = date('Y-m-d');
            $data['approved']       = 1;
            $data['flag']           = 1;

            $this->db->insert($this->_table_header, $data);
        }
     public function idTransaksiTerakhir()
        {
            $query  = $this->db->query(
                "SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_pembelian_h DESC LIMIT 0,1"
            );
            $data_id = $query->result();

            foreach ($data_id as $data) {
                $last_id = $data->id_pembelian_h;
            }

            return $last_id;
        }

    public function tampilDataPembelianDetail($id)
        {
            $query  = $this->db->query(
                "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_pembelian_h = '$id'"
            );
            return $query->result();    
        }

    public function savePembelianDetail($id)
        {
            $qty    = $this->input->post('qty');
            $harga  = $this->input->post('harga');

            $data['id_pembelian_h'] = $id;
            $data['kode_barang']    = $this->input->post('kode_barang');
            $data['qty']            = $qty;
            $data['harga']          = $harga;
            $data['jumlah']         = $qty * $harga;
            $data['flag']           = 1;

            $this->db->insert($this->_table_detail, $data);
        }
// public function tampillaporanpembelian($tgl_awal,$tgl_akhir)
public function tampillaporanpembelian($tgl_awal, $tgl_akhir)

    {
        // print_r($tgl_awal); die();
        // $this->db->select('barang.stock, pembelian_detail.qty, pembelian_detail.jumlah, pembelian_header.tanggal, pembelian_header.no_transaksi, pembelian_header.id_pembelian_h');
        // $this->db->from('pembelian_header');
        // $this->db->join('pembelian_detail', 'pembelian_detail.id_pembelian_h = pembelian_header.id_pembelian_h');
        // $this->db->join('barang', 'pembelian_detail.kode_barang = barang.kode_barang');
        // // $this->db->where('tanggal >=', $tgl_awal);
        // //  $this->db->where('tanggal <=', $tgl_akhir);
        // $this->db->group_by('pembelian_header.no_transaksi','asc');
        $this->db->select(' ph.id_pembelian_h, ph.no_transaksi,ph.tanggal, COUNT(pd.kode_barang) AS total_barang, SUM(pd.qty) as total_qty, SUM(pd.jumlah) as total_pembelian '); 
        $this->db->FROM ('pembelian_header ph');
        $this->db->JOIN ('pembelian_detail pd', 'ph.id_pembelian_h = pd.id_pembelian_h');
        $this->db->where("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
        $this->db->GROUP_BY('ph.no_transaksi');
        $query = $this->db->get();

        // SELECT ph.id_pembelian_h, ph.no_transaksi,ph.tanggal, COUNT(pd.kode_barang) AS kode_barang, SUM(pd.qty), SUM(pd.jumlah) as qty FROM pembelian_header AS ph INNER JOIN pembelian_detail as pd on ph.id_pembelian_h = pd.id_pembelian_h GROUP BY ph.no_transaksi ASC

             return $query->result();
          //  $_config[0] =& $config;  
          // return $_config[0];
    }
}
